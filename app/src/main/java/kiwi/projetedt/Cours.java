
package kiwi.projetedt;

import android.os.Parcel;
import android.os.Parcelable;

public class Cours implements Parcelable {
    private String date;
    private String heureDebut;
    private String heureFin;
    private String duree;
    private String matiere;
    private String enseignant;
    private String typeCours;
    private String salles;
    private String groupes;

    public Cours(String date, String heureDebut, String heureFin, String duree, String matiere, String enseignant, String typeCours, String salles, String groupes) {
        this.date = date;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
        this.duree = duree;
        this.matiere = matiere;
        this.enseignant = enseignant;
        this.typeCours = typeCours;
        this.salles = salles;
        this.groupes = groupes;
    }

    public Cours(Parcel in) {
        date = in.readString();
        heureDebut = in.readString();
        heureFin= in.readString();
        matiere = in.readString();
        enseignant = in.readString();
        duree = in.readString();
        typeCours = in.readString();
        salles  = in.readString();
    }


    public static final Creator<Cours> CREATOR = new Creator<Cours>() {
        @Override
        public Cours createFromParcel(Parcel in) {
            return new Cours(in);
        }

        @Override
        public Cours[] newArray(int size) {
            return new Cours[size];
        }
    };

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(String enseignant) {
        this.enseignant = enseignant;
    }

    public String getTypeCours() {
        return typeCours;
    }

    public void setTypeCours(String typeCours) {
        this.typeCours = typeCours;
    }

    public String getSalles() {
        return salles;
    }

    public void setSalles(String salles) {
        this.salles = salles;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(String heureDebut) {
        this.heureDebut = heureDebut;
    }

    public String getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(String heureFin) {
        this.heureFin = heureFin;
    }

    public String getGroupes()
    {
        return groupes;
    }

    public void setGroupes(String groupes)
    {
        this.groupes = groupes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(heureDebut);
        dest.writeString(heureFin);
        dest.writeString(matiere);
        dest.writeString(enseignant);
        dest.writeString(duree);
        dest.writeString(typeCours);
        dest.writeString(salles);


    }
}
