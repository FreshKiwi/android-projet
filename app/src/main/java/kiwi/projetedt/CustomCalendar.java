package kiwi.projetedt;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.DateTimeZone;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.io.TimezoneAssignment;

public abstract class CustomCalendar
{
    /** reference to the downloaded iCal file */
    private static File iCalFile;
    /** iCalendar containing all events */
    private static ICalendar iCal;
    /** list of today's events */
    private static List<VEvent> eventsFromToday;
    /** list of all courses, sorted by date (42nd index contains 42nd day's events);
     *  index 0 contains exams */
    private static List<List<Cours>> allClassesList;


    public static void init()
    {
        // check if file has been initialized
        if (iCalFile != null)
        {
            allClassesList = new ArrayList<>();
            // populate classes list
            for (int i = 0; i <= 365; i++)
                allClassesList.add(new ArrayList<Cours>());

            // read events from file and store them
            iCal = parseICal(iCalFile);
            // get today's events that start at most 15 minutes before now
            eventsFromToday = checkEventsTime(parseEvents(iCal));
        }
    }


    /**
     * reads an iCalendar from an iCal file, and format it using Paris' timezone
     * @param iCalFile the iCal file to parse
     * @return an ICalendar object containing all events
     */
    private static ICalendar parseICal(File iCalFile)
    {
        ICalendar iCal = null;
        try
        {
            // parse iCal file
            iCal = Biweekly.parse(iCalFile).first();

            // create a Java TimeZone object
            TimeZone timeZone = TimeZone.getTimeZone("Europe/Paris");
            String globalID = "Europe/Paris";
            // build a Biweekly TimezoneAssignment object
            TimezoneAssignment paris = new TimezoneAssignment(timeZone, globalID);
            // assign this TimezoneAssignment to the iCal
            iCal.getTimezoneInfo().setDefaultTimezone(paris);
        }
        catch (IOException | NullPointerException e)
        {
            e.printStackTrace();
        }

        return iCal;
    }


    /**
     * get today's events
     * also converts, sorts and stores events in allClassesList
     * uses Joda library
     * @param iCal the ICalendar object to get the events from
     * @return the list of today's upcoming events
     */
    private static List<VEvent> parseEvents(ICalendar iCal)
    {
        List<VEvent> events = new ArrayList<>();

        // used to create a new Cours
        String summary;

        // used to store event's date
        DateTime eventStart;

        // get current date
        DateTime now = DateTime.now().withZone(DateTimeZone.forID("Europe/Paris"));

        // for each event
        for (VEvent event : iCal.getEvents())
        {
            // get summary
            summary = event.getSummary().getValue();

            // if event is a day off, ignore it
            if ((summary.equals("Férié") || summary.equals("Vacances")))
                continue;

            // get start date with correct time zone
            eventStart = new DateTime(event.getDateStart().getValue())
                            .withZone(DateTimeZone.forID("Europe/Paris"));

            // if event happens today
            if (DateTimeComparator.getDateOnlyInstance().compare(now, eventStart) == 0)
                // add it to the list
                addEvent(event, events);

            // convert event to Cours
            Cours c = convertEvent(event);

            // add Cours to list
            addCours(c, allClassesList.get(eventStart.getDayOfYear()));
            // if event happens after now and is an exam
            if (eventStart.isAfterNow() && c.getTypeCours().equals("Evaluation"))
                allClassesList.get(0).add(c);
        }

        return events;
    }


    /**
     * adds given Cours to given list, according to dates (earliest Cours come first)
     * @param cours     the Cours to add
     * @param coursList the list to update
     */
    private static void addCours(Cours cours, List<Cours> coursList)
    {
        // if list is empty
        if (coursList.size() == 0)
        {
            // add Cours to list
            coursList.add(cours);
            return;
        }

        // iterate through list
        for (int i = 0; i < coursList.size(); i++)
        {
            // get current Cours
            Cours c = coursList.get(i);

            // if Cours happens after current Cours
            if (Integer.parseInt(cours.getHeureDebut().substring(0, 2)) >
                Integer.parseInt(c.getHeureDebut().substring(0, 2)))
                continue;

            coursList.add(i, cours);
            break;
        }
    }

    /**
     * adds given VEvent to given list, according to its date (earliest events come first)
     * @param event     the event to add
     * @param eventList the list to update
     */
    private static void addEvent(VEvent event, List<VEvent> eventList)
    {
        // if list is empty
        if (eventList.size() == 0)
        {
            // add event to list
            eventList.add(event);
            return;
        }

        // event's start date
        DateTime eventStart = new DateTime(event.getDateStart().getValue())
                .withZone(DateTimeZone.forID("Europe/Paris"));

        // iterate through list
        for (int i = 0; i < eventList.size(); i++)
        {
            // get current event
            VEvent e = eventList.get(i);

            // if event to add happens after current event
            if (eventStart.isAfter(new DateTime(e.getDateStart().getValue()).withZone(DateTimeZone.forID("Europe/Paris"))))
                continue;

            eventList.add(i, event);
            break;
        }
    }


    /**
     * check if today's events start at most 15 minutes before now
     * @param eventsFromToday list of today's events
     * @return list of today's events to display
     */
    private static List<VEvent> checkEventsTime(List<VEvent> eventsFromToday)
    {
        // list of events to return
        List<VEvent> events = new ArrayList<>();

        // get current date
        DateTime now = DateTime.now().withZone(DateTimeZone.forID("Europe/Paris"));

        // used to store event's date
        DateTime eventStart;

        // for each event
        for (VEvent event : eventsFromToday)
        {
            // get event's start date
            eventStart = new DateTime(event.getDateStart().getValue())
                            .withZone(DateTimeZone.forID("Europe/Paris"));

            // if event starts at most 15 minutes before now
            if (eventStart.minuteOfDay().get() - now.minuteOfDay().get() > -15)
                // store event
                events.add(event);
        }

        return events;
    }


    /**
     * convert a VEvent in a Cours
     * @param event the VEvent to convert
     * @return null if VEvent is a day off; a Cours otherwise
     */
    private static Cours convertEvent(VEvent event)
    {
        // get start and end dates
        DateTime eventStart = new DateTime(event.getDateStart().getValue())
                                .withZone(DateTimeZone.forID("Europe/Paris"));
        DateTime eventEnd = new DateTime(event.getDateEnd().getValue())
                                .withZone(DateTimeZone.forID("Europe/Paris"));

        // get summary
        String summary = event.getSummary().getValue();

        // if event is a day off, ignore it
        if ((summary.equals("Férié") || summary.equals("Vacances")))
            return null;

        // get course name, teachers, classes, and course type
        String[] summaryData = summary.split(" - ");

        // new Cours
        Cours c;

        // if (summaryData.length == 4)
        switch (summaryData.length)
        {
            case 4:
                // instantiate new Cours
                c = new Cours(
                        // date
                        eventStart.toString(DateTimeFormat.forPattern("dd/MM/yyyy")),
                        // heureDebut
                        eventStart.toString(DateTimeFormat.forPattern("HH:mm")),
                        // heureFin
                        eventEnd.toString(DateTimeFormat.forPattern("HH:mm")),
                        // duree
                        Hours.hoursBetween(eventStart, eventEnd).getHours() % 24 + "h"
                                + Minutes.minutesBetween(eventStart, eventEnd).getMinutes() % 60,
                        // matiere
                        summaryData[0],
                        // enseignant
                        summaryData[1],
                        // typeCours
                        summaryData[3],
                        // salles; if no location provided, fill with "n/a"
                        event.getLocation() != null ? event.getLocation().getValue() : "n/a",
                        // groupes
                        summaryData[2]
                );
                break;

            case 3:
                // instantiate new Cours
                c = new Cours(
                        // date
                        eventStart.toString(DateTimeFormat.forPattern("dd/MM/yyyy")),
                        // heureDebut
                        eventStart.toString(DateTimeFormat.forPattern("HH:mm")),
                        // heureFin
                        eventEnd.toString(DateTimeFormat.forPattern("HH:mm")),
                        // duree
                        Hours.hoursBetween(eventStart, eventEnd).getHours() % 24 + "h"
                                + Minutes.minutesBetween(eventStart, eventEnd).getMinutes() % 60,
                        // matiere
                        summaryData[0],
                        // enseignant; here: not provided
                        "n/a",
                        // typeCours
                        summaryData[2],
                        // salles; if no location provided, fill with "n/a"
                        event.getLocation() != null ? event.getLocation().getValue() : "n/a",
                        // groupes
                        summaryData[1]
                );
                break;

            default:
                // instantiate new Cours
                c = new Cours(
                        // date
                        eventStart.toString(DateTimeFormat.forPattern("dd/MM/yyyy")),
                        // heureDebut
                        eventStart.toString(DateTimeFormat.forPattern("HH:mm")),
                        // heureFin
                        eventEnd.toString(DateTimeFormat.forPattern("HH:mm")),
                        // duree
                        Hours.hoursBetween(eventStart, eventEnd).getHours() % 24 + "h"
                                + Minutes.minutesBetween(eventStart, eventEnd).getMinutes() % 60,
                        // matiere
                        summaryData[0],
                        // enseignant; here: not provided
                        "n/a",
                        // typeCours; here: not provided
                        "n/a",
                        // salles; if no location provided, fill with "n/a"
                        event.getLocation() != null ? event.getLocation().getValue() : "n/a",
                        // groupes
                        summaryData[1]
                );
                break;
        }

        return c;
    }






    /* GETTERS AND SETTERS */

    public static ICalendar getiCal()
    {
        return iCal;
    }

    public static File getiCalFile()
    {
        return iCalFile;
    }

    public static void setiCalFile(File iCalFile)
    {
        CustomCalendar.iCalFile = iCalFile;
    }

    /**
     * usage: getCoursFromDay(DateTime.now().dayOfYear())
     * @param day the day of the year
     * @return a list containing all Cours from the given day
     */
    public static List<Cours> getCoursFromDay(int day)
    {
        return allClassesList.get(day);
    }



    public static List<Cours> getExams()
    {
        return allClassesList.get(0);
    }
}
