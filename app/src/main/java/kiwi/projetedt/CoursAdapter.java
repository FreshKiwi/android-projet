package kiwi.projetedt;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class CoursAdapter extends RecyclerView.Adapter<CoursAdapter.CoursViewHolder> {


    public List<Cours> mCours;

    public CoursAdapter(List<Cours> datas){
        mCours = datas;
    }
    @NonNull
    @Override
    public CoursViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.class_day,viewGroup, false);
        return new CoursViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CoursViewHolder coursViewHolder, int i) {
        Cours cours = mCours.get(i);
        coursViewHolder.bind(cours);
    }

    @Override
    public int getItemCount() {
        return mCours.size();
    }

    public static class CoursViewHolder extends RecyclerView.ViewHolder{

        CardView coursCard;
        TextView tempsCours;
        TextView dateCours;
        TextView matiereCours;
        TextView enseignantCours;
        TextView salleCours;
        TextView dureeCours;


        public CoursViewHolder(@NonNull View itemView) {
            super(itemView);
            coursCard = itemView.findViewById(R.id.cours_card);
            tempsCours =  itemView.findViewById(R.id.time_cours);
            dateCours = itemView.findViewById(R.id.date_cours);
            matiereCours = itemView.findViewById(R.id.matiere_cours);
            enseignantCours =itemView.findViewById(R.id.enseignant_cours);
            salleCours = itemView.findViewById(R.id.salle_cours);
            dureeCours = itemView.findViewById(R.id.duree_cours);
        }

        public void bind(Cours cours){
            tempsCours.setText(cours.getHeureDebut() + "-" + cours.getHeureFin() + " / " + cours.getTypeCours() );
            dateCours.setText("Date : " + cours.getDate());
            matiereCours.setText("Matiere : " + cours.getMatiere(   ));
            enseignantCours.setText("Enseignant : " + cours.getEnseignant());
            salleCours.setText("Salle(s) : " + cours.getSalles());
            dureeCours.setText("Durée : " +cours.getDuree());
        }

    }
}


