package kiwi.projetedt;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.io.TimezoneAssignment;
import biweekly.io.TimezoneInfo;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, TodayCoursesFragment.OnFragmentInteractionListener, CoursesOfTheDay.OnFragmentInteractionListener , CalendarCoursesFragment.OnFragmentInteractionListener, Preferences.OnFragmentInteractionListener{

    //FOR DESIGN
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    // unique download ID
    private long downloadID;
    // broadcast receiver to catch notifications of downloads complete
    private BroadcastReceiver onDownloadComplete;

    private SharedPreferences sharedPreferences;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = this.getPreferences(Context.MODE_PRIVATE);

        // reference to the downloaded iCal file
        CustomCalendar.setiCalFile(new File(getExternalFilesDir(null), "iCal"));

        // download iCal file with all groups
        int defaultValue = getResources().getInteger(R.integer.defaultgroup);
        int group = sharedPreferences.getInt(getString(R.string.group_choice), defaultValue);
        int[] groups = new int[] {group};
        this.downloadICal(groups);

        // 6 - Configure all views

        this.configureToolBar();

        this.configureDrawerLayout();

        this.configureNavigationView();


        if (savedInstanceState == null) {
            loadFragment(new TodayCoursesFragment());
            navigationView.setCheckedItem(R.id.nav_home);
        }
        //this.configureHorizontalCalendarView();


    }



    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        // stop listening for download complete broadcasts when activity is destroyed
        unregisterReceiver(onDownloadComplete);
    }

    @Override
    public void onBackPressed() {
        // 5 - Handle back click to close menu
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        // 4 - Handle Navigation Item Click
        int id = item.getItemId();

        switch (id){
            case R.id.nav_home :
                loadFragment(TodayCoursesFragment.newInstance(CustomCalendar.getCoursFromDay(
                        DateTime.now().withZone(DateTimeZone.forID("Europe/Paris")).dayOfYear().get())));
                navigationView.setCheckedItem(R.id.nav_home);
                break;
            case R.id.nav_today :
                loadFragment( CoursesOfTheDay.newInstance(CustomCalendar.getCoursFromDay(
                        DateTime.now().withZone(DateTimeZone.forID("Europe/Paris")).dayOfYear().get())));
                navigationView.setCheckedItem(R.id.nav_today);
                break;
            case R.id.nav_day :
                loadFragment( new CalendarCoursesFragment());
                navigationView.setCheckedItem(R.id.nav_day);
                break;
            case R.id.nav_prefs :
                loadFragment(new Preferences());
                navigationView.setCheckedItem(R.id.nav_prefs);
                break;
            case R.id.nav_exams :
                loadFragment( CoursesOfTheDay.newInstance(CustomCalendar.getExams()));
                navigationView.setCheckedItem(R.id.nav_exams);
                break;
            default:
                break;
        }

        this.drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    // ---------------------
    // CONFIGURATION
    // ---------------------

    // 1 - Configure Toolbar
    private void configureToolBar(){
        this.toolbar = findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);
    }

    // 2 - Configure Drawer Layout
    private void configureDrawerLayout(){
        this.drawerLayout = findViewById(R.id.activity_main_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    // 3 - Configure NavigationView
    private void configureNavigationView(){
        this.navigationView = findViewById(R.id.activity_main_nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void loadFragment(Fragment fragment) {
         // create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit(); // save the changes
    }


    private void configureHorizontalCalendarView(){
        // starts now
        Calendar startDate = Calendar.getInstance();

        // ends after 1 month from now
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        // setup calendar
        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .build();

        // listen to date changes
        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                // TODO: update events view
            }
        });
    }



    /**
     * downloads iCal file as an external file for the given groups
     * also setups a broadcast receiver to parse the file once downloaded
     * @param groups the selected groups
     */
   public void downloadICal(int[] groups)
    {
        // create broadcast receiver to catch notifications of downloads complete
        this.onDownloadComplete = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                //Fetching the download id received with the broadcast
                long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                //Checking if the received broadcast is for our enqueued download by matching download id
                if (downloadID == id)
                {
                    // display notification
                    Toast.makeText(MainActivity.this, "Download completed", Toast.LENGTH_SHORT).show();
                    // parse iCal file
                    CustomCalendar.init();
                    // update view
                    loadFragment(TodayCoursesFragment.newInstance(CustomCalendar.getCoursFromDay(
                            DateTime.now().withZone(DateTimeZone.forID("Europe/Paris")).dayOfYear().get())));
                }
            }
        };
        registerReceiver(onDownloadComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        // build URL to download iCal file
        StringBuilder url;
        // if no group selected
        if (groups == null)
            url = new StringBuilder(getString(R.string.ical_url));
        // get URL with all requested groups
        else
        {
            url = new StringBuilder(getString(R.string.grp_baseURL));
            for (int grp : groups)
                switch (grp)
                {
                    case 1:
                        url.append(getString(R.string.grp_1));
                        break;
                    case 2:
                        url.append(getString(R.string.grp_2));
                        break;
                    case 3:
                        url.append(getString(R.string.grp_3));
                        break;
                    case 4:
                        url.append(getString(R.string.grp_4));
                        break;
                    case 5:
                        url.append(getString(R.string.grp_5));
                        break;
                    default:
                        url = new StringBuilder(getString(R.string.ical_url));
                }
        }

        // delete previous iCal file (if exists)
        CustomCalendar.getiCalFile().delete();

        // create a DownloadManager.Request with all the information necessary to start the download
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url.toString()))
                // title of notification download
                .setTitle("iCal file")
                // description of the download notification
                .setDescription("Downloading...")
                // visibility of the download notification
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                // uri of the destination file
                .setDestinationUri(Uri.fromFile(CustomCalendar.getiCalFile()))
                // set if charging is required to begin the download; requires API level 24 (current min: 16)
                // .setRequiresCharging(false)
                // set if download is allowed on Mobile network
                .setAllowedOverMetered(true)
                // set if download is allowed on roaming network
                .setAllowedOverRoaming(true);

        // DownloadManager: separate system service downloading files requested by client
        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        // initiate download by putting it in DownloadManager's queue
        this.downloadID = downloadManager.enqueue(request);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}