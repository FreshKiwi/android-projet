package kiwi.projetedt;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CoursesOfTheDay.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CoursesOfTheDay#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CoursesOfTheDay extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";


    private List<? extends Cours> mParam1;

    //For class management

    private OnFragmentInteractionListener mListener;
    private RecyclerView classRecyclerView;

    public CoursesOfTheDay() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment CoursesOfTheDay.
     */
    // TODO: Rename and change types and number of parameters
    public static CoursesOfTheDay newInstance(List<Cours> param1) {
        CoursesOfTheDay fragment = new CoursesOfTheDay();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, (ArrayList<? extends Parcelable>) param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelableArrayList(ARG_PARAM1);
        }
        else
            updateData();
    }

    private boolean updateData() {
        ArrayList<Cours> test = new ArrayList<>();
        test.add(new Cours("","","","","","","","",""));
        test.add(new Cours("","","","","","","","",""));
        test.add(new Cours("","","","","","","","",""));
        test.add(new Cours("","","","","","","","",""));
        test.add(new Cours("","","","","","","","",""));
        test.add(new Cours("","","","","","","","",""));
        test.add(new Cours("","","","","","","","",""));
        mParam1 = test;
        if (mParam1 != null && !mParam1.isEmpty()) {

            return true;
        }

        return false;
    }

    private void updateView(){
        classRecyclerView.setAdapter(new CoursAdapter((List<Cours>) mParam1));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_courses_of_the_day, container,false);

        this.classRecyclerView = myFragmentView.findViewById(R.id.recycler_view_cours);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        classRecyclerView.setLayoutManager(llm);
        if(mParam1 != null){
            updateView();
        }
        return myFragmentView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
