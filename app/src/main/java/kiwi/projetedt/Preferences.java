package kiwi.projetedt;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Preferences.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Preferences#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Preferences extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private SharedPreferences sharedPreferences;

    private RadioGroup tdRadio;
    private OnFragmentInteractionListener mListener;

    public Preferences() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Preferences.
     */

    public static Preferences newInstance(String param1, String param2) {
        Preferences fragment = new Preferences();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myFragmentView = inflater.inflate(R.layout.fragment_preferences, container,false);
        tdRadio = myFragmentView.findViewById(R.id.radio_group_td);
        tdRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                onRadioButtonClicked(group, checkedId);
            }
        });
        return myFragmentView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void onRadioButtonClicked(RadioGroup view, int checkedId) {
        // Is the button now checked?
        int checkedRadioId = view.getCheckedRadioButtonId();
        int button =0;
        // Check which radio button was clicked
        switch(checkedRadioId) {
            case R.id.radioButtonTD1:
                button = 1;
                Toast.makeText(getContext(), "TD1 sauvegardé", Toast.LENGTH_SHORT).show();
                break;
            case R.id.radioButtonTD2:
                button = 2;
                    Toast.makeText(getContext(), "TD2 sauvegardé", Toast.LENGTH_SHORT).show();
                    break;
            case R.id.radioButtonTD3:
                button = 3;
                    Toast.makeText(getContext(), "TD3 sauvegardé", Toast.LENGTH_SHORT).show();
                    break;
            case R.id.radioButtonTD4:
                button = 4;
                    Toast.makeText(getContext(), "TD4 sauvegardé", Toast.LENGTH_SHORT).show();
                    break;
            case R.id.radioButtonTD5:
                button = 5;
                    Toast.makeText(getContext(), "TD5 sauvegardé", Toast.LENGTH_SHORT).show();
                    break;
            case R.id.radioButtonTD6:
                button = 6;
                    Toast.makeText(getContext(), "TD6 sauvegardé", Toast.LENGTH_SHORT).show();
                    break;
        }
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.group_choice), button );
        editor.commit();

        int defaultValue = getResources().getInteger(R.integer.defaultgroup);
        int group = sharedPref.getInt(getString(R.string.group_choice), defaultValue);
        int[] groups = new int[] {group};

        ((MainActivity)getActivity()).downloadICal(groups);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
