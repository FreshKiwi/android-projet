package kiwi.projetedt;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TodayCoursesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TodayCoursesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TodayCoursesFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";


    private List<? extends Cours> mParam1;

    //For class management
    private TextView tempsCours1, matiereCours, salleCours, dureeCours, enseignantCours,  tempsCours2, matiereCours2, enseignantCours2 ;
    private Cours mCours1, mCours2;
    private OnFragmentInteractionListener mListener;


    public TodayCoursesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment TodayCoursesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TodayCoursesFragment newInstance(List<Cours> param1) {
        TodayCoursesFragment fragment = new TodayCoursesFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, (ArrayList<? extends Parcelable>) param1);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelableArrayList(ARG_PARAM1);
            Log.d("info","" +mParam1.get(0)+"");
            mCours1 = mParam1.get(0);
            mCours2 = mParam1.get(1);
        }
        else
            updateData();
    }

    private boolean updateData() {
        //TODO: RetrieveData
        ArrayList<Cours> test = new ArrayList<>();
        test.add(new Cours("","","","","","","","",""));
        test.add(new Cours("","","","","","","","",""));

        mParam1 = test;
        if (mParam1 != null && !mParam1.isEmpty()) {
            mCours1 = mParam1.get(0);

            mCours2 = mParam1.get(1);

            return true;

        }
        return false;

    }

    public void updateView(){
        //update current class
        tempsCours1.setText(mCours1.getHeureDebut() + "-" + mCours1.getHeureFin() + " / " + mCours1.getTypeCours() );
        matiereCours.setText("Matiere : " + mCours1.getMatiere());
        enseignantCours.setText("Enseignant : " + mCours1.getEnseignant());
        salleCours.setText("Salle(s) : " + mCours1.getSalles());
        dureeCours.setText("Durée : " +mCours1.getDuree());

        //update cours suivant

        tempsCours2.setText(mCours2.getHeureDebut() + "-" + mCours2.getHeureFin() + " / " + mCours2.getTypeCours() );
        matiereCours2.setText("Matiere : " + mCours2.getMatiere());
        enseignantCours2.setText("Salle(s) : " + mCours2.getSalles());

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myFragmentView = inflater.inflate(R.layout.fragment_today_courses, container,false);

        tempsCours1 = myFragmentView.findViewById(R.id.time_cours);
        matiereCours = myFragmentView.findViewById(R.id.matiere_cours);
        enseignantCours = myFragmentView.findViewById(R.id.enseignant_cours);
        salleCours = myFragmentView.findViewById(R.id.salle_cours);
        dureeCours = myFragmentView.findViewById(R.id.duree_cours);

        tempsCours2 = myFragmentView.findViewById(R.id.time_cours_suivant);
        matiereCours2 =myFragmentView.findViewById(R.id.matiere_cours_suivant);
        enseignantCours2 =myFragmentView.findViewById(R.id.enseignant_cours_suivant);

        if(mCours1 != null && mCours2 != null)
            updateView();
        return myFragmentView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void configureRecyclerView() {

    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
